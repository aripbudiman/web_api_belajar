<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        $result = Order::create($request->all());
        return response()->json([
            'success' => true,
            'data' => $result
        ], 200);
    }

    public function getAllData()
    {
        $result = Order::all();
        return response()->json([
            'success' => true,
            'data' => $result
        ], 200);
    }
}
