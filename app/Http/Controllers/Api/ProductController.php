<?php

namespace App\Http\Controllers\Api;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function store(Request $request, Products $model)
    {
        $validator = Validator::make($request->all(), [
            'product_name' => 'required|unique:products,product_name',
        ], [
            'product_name.unique' => 'Product sudah ada',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation errors',
                'errors' => $validator->errors()
            ], 422);
        }
        DB::beginTransaction();
        try {
            $model->create($request->all());
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Product created successfully'
            ], 200);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Product creation failed',
                'error' => $th->getMessage()
            ], 500);
        }
    }

    public function getAllProduct()
    {
        $result = Products::paginate(100);
        return response()->json(
            $result,
            200
        );
    }
}
