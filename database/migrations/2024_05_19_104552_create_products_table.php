<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_name', 128)->nullable()->default(null);
            $table->string('category', 64)->nullable()->default(null);
            $table->decimal('price', 8, 2)->nullable()->default(null);
            $table->integer('stock')->nullable()->default(null);
            $table->string('image')->nullable()->default(null);
            $table->float('rating', 5, 0)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
